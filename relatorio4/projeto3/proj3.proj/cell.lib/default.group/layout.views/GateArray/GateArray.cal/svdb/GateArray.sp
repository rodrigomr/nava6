* SPICE NETLIST
***************************************

.SUBCKT Q C B E S G
.ENDS
***************************************
.SUBCKT lddn D G S B
.ENDS
***************************************
.SUBCKT probepad pad
.ENDS
***************************************
.SUBCKT L POS NEG SUB
.ENDS
***************************************
.SUBCKT hall A B C D S P1 P2
.ENDS
***************************************
.SUBCKT GateArray VSS VDD CTR IN2 IN3 IN4 IN1 IN5
** N=17 EP=8 IP=0 FDC=32
* PORT IN3 IN3 24000 -10550 routingmet1
* PORT IN4 IN4 33050 -10550 routingmet1
* PORT VDD VDD 46525 0 routingmet4
* PORT VSS VSS 46900 -18625 routingmet4
* PORT IN5 IN5 42050 -10450 routingmet1
* PORT CTR CTR -3550 -8950 routingmet3
* PORT IN2 IN2 15000 -10650 routingmet1
* PORT IN1 IN1 5950 -10550 routingmet1
M0 VSS VSS 9 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.15e-12 PD=2.65e-06 PS=8.1e-06 NRD=0.0772727 NRS=0.0772727 $X=575 $Y=-17000 $D=22
M1 10 CTR VSS VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=3575 $Y=-17000 $D=22
M2 IN2 IN1 10 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=6575 $Y=-17000 $D=22
M3 VSS VSS IN2 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=9575 $Y=-17000 $D=22
M4 11 CTR VSS VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=12575 $Y=-17000 $D=22
M5 IN3 IN2 11 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=15575 $Y=-17000 $D=22
M6 VSS VSS IN3 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=18575 $Y=-17000 $D=22
M7 12 CTR VSS VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=21575 $Y=-17000 $D=22
M8 IN4 IN3 12 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=24575 $Y=-17000 $D=22
M9 VSS VSS IN4 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=27575 $Y=-17000 $D=22
M10 13 CTR VSS VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=30575 $Y=-17000 $D=22
M11 IN5 IN4 13 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=33575 $Y=-17000 $D=22
M12 VSS VSS IN5 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=36575 $Y=-17000 $D=22
M13 14 CTR VSS VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=39575 $Y=-17000 $D=22
M14 IN1 IN5 14 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=42575 $Y=-17000 $D=22
M15 15 VSS IN1 VSS nmos4 L=3.5e-07 W=5.5e-06 AD=7.15e-12 AS=7.2875e-12 PD=8.1e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=45575 $Y=-17000 $D=22
M16 VDD VDD 16 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.15e-12 PD=2.65e-06 PS=8.1e-06 NRD=0.0772727 NRS=0.0772727 $X=575 $Y=-8000 $D=26
M17 IN2 CTR VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=3575 $Y=-8000 $D=26
M18 VDD IN1 IN2 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=6575 $Y=-8000 $D=26
M19 IN3 CTR VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=12575 $Y=-8000 $D=26
M20 VDD IN2 IN3 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=15575 $Y=-8000 $D=26
M21 IN4 CTR VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=21575 $Y=-8000 $D=26
M22 VDD IN3 IN4 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=24575 $Y=-8000 $D=26
M23 IN5 CTR VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=30575 $Y=-8000 $D=26
M24 VDD IN4 IN5 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=33575 $Y=-8000 $D=26
M25 IN1 CTR VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=39575 $Y=-8000 $D=26
M26 VDD IN5 IN1 VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=42575 $Y=-8000 $D=26
M27 17 VDD VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.15e-12 AS=7.2875e-12 PD=8.1e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=45575 $Y=-8000 $D=26
M28 VDD VDD VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=9575 $Y=-8000 $D=27
M29 VDD VDD VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=18575 $Y=-8000 $D=27
M30 VDD VDD VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=27575 $Y=-8000 $D=27
M31 VDD VDD VDD VDD pmos4 L=3.5e-07 W=5.5e-06 AD=7.2875e-12 AS=7.2875e-12 PD=2.65e-06 PS=2.65e-06 NRD=0.0772727 NRS=0.0772727 $X=36575 $Y=-8000 $D=27
.ENDS
***************************************
